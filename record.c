typedef struct {
	long time;
	long right_motor_speed;
	long left_motor_speed;
	long arm_speed;
	long claw_speed;
} robot_task;

#define ROBOT_TASK_NUM 5
robot_task robot_tasks[ROBOT_TASK_NUM];
robot_task cached_values;

// whether or not to cache the current values
int cache = 1;
// whether or not to record the motor values
int record = 0;

void save_cached_to_task() {
	static t = 0;
	if(t >= ROBOT_TASK_NUM) return;
	robot_tasks[t].time = time1[T1];
	robot_tasks[t].right_motor_speed = cached_values.right_motor_speed;
	robot_tasks[t].left_motor_speed = cached_values.left_motor_speed;
	robot_tasks[t].arm_speed = cached_values.arm_speed;
	robot_tasks[t].claw_speed = cached_values.claw_speed;
	t++;
}

void cache_current_values() {
	clearTimer(T1);
	cached_values.right_motor_speed = motor[rightMotor];
	cached_values.left_motor_speed = motor[leftMotor];
	cached_values.arm_speed = motor[armMotor];
	cached_values.claw_speed = motor[clawMotor];
}


task usercontrol() {
	// here would be the user control code

	// recording code
	// TODO: change this to proper button
	if(vexRT[Btn5U]) {
		record = !record;
		cache_current_values();
	}

	if(record) {
		// if the timer is over 30,000 ms
		if(	time1[T1] >= 30000 ||
			motor[rightMotor] != cached_values.right_motor_speed ||
			motor[leftMotor] != cached_values.left_motor_speed ||
			motor[armMotor] != cached_values.arm_speed ||
			motor[clawMotor] != cached_values.claw_speed) {
			// if any values have changed then we want to
			// save the old values and then re cache the current values
			save_cached_to_task();
			cache_current_values();
		}
	}
}